
function init_my_daterangepicker()
{
    var today = new Date();
    var lastMonth = new Date(today.getFullYear(), today.getMonth()-1, today.getDate());
    $('.__daterangepicker').daterangepicker({
        startDate: lastMonth.toLocaleDateString(),
        endDate: today.toLocaleDateString(),
        locale: {
          applyLabel: '決定',
          cancelLabel: 'クリア',
        },
    });
}


function init_my_datepicker() {
    if( typeof ($.fn.daterangepicker) === 'undefined') {
        return;
    }

    $('.__datepicker').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_1",
        locale: {
            // ref: daterangepicker_ja.js line:722
            cancelLabel: 'クリア',
        },
    }, function(start, end, label) {
    });
}


function init_my_datatable()
{
    if ($('.__datatable').length) {
        $('.__datatable').DataTable({
          dom: "Blfrtip",
          buttons: [
            {
              extend: "copy",
              className: "btn-sm"
            },
            {
              extend: "csv",
              className: "btn-sm"
            },
            {
              extend: "excel",
              className: "btn-sm"
            },
            {
              extend: "pdfHtml5",
              className: "btn-sm"
            },
            {
              extend: "print",
              className: "btn-sm"
            },
          ],
          fixedHeader: true,
        });
      }
}


$(function(){
    init_my_daterangepicker();
    init_my_datatable();
    init_my_datepicker();
});
